package logparser

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
)

// check handles error checking
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// GetPossition reads in value from a state file and returns the value as an int
func GetPossition(path string) int64 {
	f, error := ioutil.ReadFile(path)
	if error != nil {
		// state file must be missing / deleted or this is the first execution
		// set start possition as 0
		return 0
	}
	i, _ := strconv.ParseInt(string(f), 10, 64)
	return i
}

// UpdatePossition writes to the state file the possition of the last line in a file
func UpdatePossition(path string, v int64) {
	val := strconv.Itoa(int(v))
	error := ioutil.WriteFile(path, []byte(val), 0644)
	check(error)
}

// IsNewFile checks the read possition against the current file length to determine
// if the logfile is new.  Some data loss but minimal if run every 15 seconds
func IsNewFile(prev int64, current int64) bool {
	if current < prev {
		return true
	}
	return false
}

// FileSize will return the size of the file
func FileSize(p string) int64 {
	f, err := os.Stat(p)
	check(err)

	size := f.Size()
	return size
}

// SearchString looks at a string and returns true if it matches a given regex string
func SearchString(s string, m string) bool {
	pattern := regexp.MustCompile(m)
	result := pattern.FindAllStringSubmatch(s, -1)
	if result != nil {
		return true
	}
	return false
}

// SearchNewLines takes a file path and reads the file one line at a time into a byte slice
// from a given start point.
func SearchNewLines(path string, start int64, searchpattern string) int {
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	var count int

	file.Seek(start, 0)

	reader := bufio.NewReader(file)

	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}

		if SearchString(string(line), searchpattern) {
			count++
		}
	}

	return count
}
