SHELL = /bin/bash

TARGETS := cleanup \
	test \
	macparser \
	logparser \
	run

clean:
	@echo Cleaning up...
	@rm -f ./test ./macparser ./parser_statefile ./logparser

test:
	@go test ./... -cover -bench=.

macparser: clean
	@echo Building mac package
	@go build -o macparser

logparser: clean
	@echo Building linux package
	@export GOOS=linux; export GOARCH=amd64; go build -o logparser

run: clean macparser
	@echo Running on local files, expect \'13\'
	@./macparser

