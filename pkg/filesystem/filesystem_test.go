package filesystem

import (
	"reflect"
	"testing"
)

const path string = "./testfiles"
const pattern string = "pok_publisher"

func TestFilterByRegex(t *testing.T) {
	expected := []string{
		"pok_publisher.gueapokapptst03.skybet.net.log.20190212_06",
		"pok_publisher.gueapokapptst03.skybet.net.log.20190212_07",
		"pok_publisher.gueapokapptst03.skybet.net.log.20190212_08",
		"pok_publisher.gueapokapptst03.skybet.net.log.20190212_09"}

	test := FilterByRegex(allFilesInPath(path), pattern)
	actual := []string{}

	for _, file := range test {
		actual = append(actual, file.Name())
	}

	if reflect.DeepEqual(actual, expected) != true {
		t.Errorf("Expected %s and got %s", expected, actual)
	}
}

func TestFindCurrentLog(t *testing.T) {
	expected := "./testfiles/pok_publisher.gueapokapptst03.skybet.net.log.20190212_09"
	actual := FindCurrentLog(path, pattern)

	if expected != actual {
		t.Errorf("Expected %s and got %s", expected, actual)
	}

}
