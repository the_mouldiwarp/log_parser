package filesystem

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"time"
)

// allFilesInPath returns a slice of FileInfo objects
func allFilesInPath(path string) []os.FileInfo {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	return files
}

// findMostRecentModFile will take a slice of FileInfo objects and return the most recently modified
func findMostRecentModFile(path string, files []os.FileInfo) string {

	type f struct {
		name string
		mod  time.Time
		diff time.Duration
	}

	var baseduration time.Duration = time.Hour * 10000
	l := f{name: "empty", mod: time.Now().Add(time.Hour * 24), diff: baseduration}

	for _, f := range files {
		now := time.Now()
		if now.Sub(f.ModTime()) < l.diff {
			l.name = f.Name()
			l.mod = f.ModTime()
		}
	}

	return l.name

}

// FilterByRegex takes a path and filters the content by a regex match
func FilterByRegex(list []os.FileInfo, match string) []os.FileInfo {
	var files []os.FileInfo
	pattern := regexp.MustCompile(match)

	for _, filename := range list {
		result := pattern.FindAllStringSubmatch(filename.Name(), -1)
		if result != nil {
			files = append(files, filename)
		}
	}
	return files
}

// FindCurrentLog takes an array of filemanes and identifies the most recent / active
func FindCurrentLog(path string, match string) string {
	list := FilterByRegex(allFilesInPath(path), match)

	return fmt.Sprintf("%s/%s", path, findMostRecentModFile(path, list))
}
