package logparser

import (
	"os"
	"strconv"
	"testing"
)

const path = "./testfiles/sample.log"
const statefile = "./testfiles/parser_location"

func TestGetPossitionWithFile(t *testing.T) {
	var expected int64 = 12919

	actual := GetPossition(statefile)

	if actual != expected {
		t.Errorf("Expected %d and got %d", expected, actual)
	}
}

func TestGetPossitionWithoutFile(t *testing.T) {
	var expected int64

	actual := GetPossition("./nofile")

	if actual != expected {
		t.Errorf("Expected %d and got %d", expected, actual)
	}
}

func TestIsNewFileFalse(t *testing.T) {
	var c int64 = 14000
	var p int64 = 10000

	expect := false
	actual := IsNewFile(p, c)

	if expect != actual {
		t.Errorf("Expected %s and got %s", strconv.FormatBool(expect), strconv.FormatBool(actual))
	}
}

func TestIsNewFileTrue(t *testing.T) {
	var c int64 = 100
	var p int64 = 14000

	expect := true
	actual := IsNewFile(p, c)

	if expect != actual {
		t.Errorf("Expected %s and got %s", strconv.FormatBool(expect), strconv.FormatBool(actual))
	}
}

func cleanup(path string) {
	_, err := os.Stat(path)
	if err == nil {
		os.Remove(path)
	}
}

func TestUpdatePossition(t *testing.T) {

	path := "./test_possition"
	var expect int64 = 2121

	cleanup(path)

	UpdatePossition(path, expect)

	actual := GetPossition(path)

	if expect != actual {
		t.Errorf("Expected %d and got %d", expect, actual)
	}

	cleanup(path)

}

func BenchmarkUpdatePossition(t *testing.B) {
	UpdatePossition(statefile, 12919)
}

func TestFileSize(t *testing.T) {
	var expected int64 = 12919
	actual := FileSize(path)

	if expected != actual {
		t.Errorf("Expected %d and got %d", expected, actual)
	}
}

func Benchmark(t *testing.B) {
	FileSize(path)
}

func TestSearchStringtrue(t *testing.T) {
	teststring := "pok_publisher.gueapokapp01.skybet.net.log.20190206_05.gz:02/06-05:01:17.564419 [error] Error in process <0.30482.5> on node 'pkpublisher@gueapokapp01' with exit value: {\"Cannot connect to DB\",[{pkpub_push,get_message_id,3},{pkpub_push,send,4},{pkpub_coalesce,'-coalesce/3-fun-0-',3},{lists,foreach,2},{pkpub_coalesce,coalesce,3}]}"
	testmatch := "Cannot connect to DB"
	expecting := true
	actual := SearchString(teststring, testmatch)

	if expecting != actual {
		t.Errorf("Expected %s and got %s", strconv.FormatBool(expecting), strconv.FormatBool(actual))
	}
}

func TestSearchNewLinesFull(t *testing.T) {
	var testmatch = "Cannot connect to DB"
	var expect = 13
	var pos int64

	actual := SearchNewLines(path, pos, testmatch)

	if expect != actual {
		t.Errorf("Expected %d and got %d", expect, actual)
	}
}

func TestSearchNewLinesPartialFull(t *testing.T) {
	var testmatch = "Cannot connect to DB"
	var expect = 5
	var pos int64 = 6000

	actual := SearchNewLines(path, pos, testmatch)

	if expect != actual {
		t.Errorf("Expected %d and got %d", expect, actual)
	}
}

func BenchmarkSearchNewLinesFull(t *testing.B) {
	var pth = "./testfiles/bigfile.log"
	var testmatch = "Cannot connect to DB"
	var pos int64

	SearchNewLines(pth, pos, testmatch)
}

func BenchmarkSearchNewLinesPartial(t *testing.B) {
	var pth = "./testfiles/bigfile.log"
	var testmatch = "Cannot connect to DB"
	var pos int64 = 20000

	SearchNewLines(pth, pos, testmatch)
}
