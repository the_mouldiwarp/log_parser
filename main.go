package main

import (
	"fmt"

	"bitbucket.org/the_mouldiwarp/poker_publisher_monitor/pkg/filesystem"
	lp "bitbucket.org/the_mouldiwarp/poker_publisher_monitor/pkg/logparser"
	flag "github.com/ogier/pflag"
)

var (
	fLogPath, fLineMatch, fFileMatch, fStateFile string
)

func countDBConnectionErrors(p string, statefile string, fileMatch string, lineMatch string) int {
	targetlog := filesystem.FindCurrentLog(p, fileMatch)
	current := lp.FileSize(targetlog)
	prev := lp.GetPossition(statefile)

	if lp.IsNewFile(prev, current) {
		prev = 0
	}

	lp.UpdatePossition(statefile, current)

	return lp.SearchNewLines(targetlog, prev, lineMatch)
}

func responder(count int) {
	fmt.Printf("[{\"searchterm\":\"%s\",\"logfile\":\"%s\",\"count\":%d}]\n", fLineMatch, fFileMatch, count)
}

func main() {
	flag.Parse()
	responder(countDBConnectionErrors(fLogPath, fStateFile, fFileMatch, fLineMatch))
}

func init() {
	flag.StringVarP(&fLogPath, "Log path", "p", "./pkg/filesystem/testfiles", "Log directory, example: /opt/openbet/logs/poker/publisher")
	flag.StringVarP(&fFileMatch, "File match", "m", "pok_publisher", "Regex when identifying current logfile, example: \"pok_poblisher\"")
	flag.StringVarP(&fLineMatch, "Line match", "l", "Cannot connect to DB", "Regex to use in line match, example: \"Cannot connect to DB\"")
	flag.StringVarP(&fStateFile, "State file", "s", "./parser_statefile", "file in which to store state (last read possition), example: ./parse_position_state")
}
